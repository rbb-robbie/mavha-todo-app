import { Injectable } from "@angular/core";
import { JwtHelperService } from "@auth0/angular-jwt";
import { HttpClient } from "@angular/common/http";
import { environment } from "src/environments/environment";
import { Subject } from "rxjs";

@Injectable({
  providedIn: "root"
})
export class AuthService {
  constructor(
    protected jwtHelper: JwtHelperService,
    protected http: HttpClient
  ) {}

  public static getToken() {
    return localStorage.getItem("access_token");
  }
  public isAuthenticated(): boolean {
    const token = localStorage.getItem("access_token");
    return !this.jwtHelper.isTokenExpired(token);
  }

  public login(body) {
    const authCall = this.http.post(`${environment.apiUrl}/login_check`, body);
    const subject = new Subject();
    authCall.subscribe((auth: { token: string }) => {
      localStorage.setItem("access_token", auth.token);
      subject.next();
    });
    return subject;
  }
}
