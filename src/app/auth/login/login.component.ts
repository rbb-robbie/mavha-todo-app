import { Component, OnInit } from "@angular/core";
import { AuthService } from "../auth.service";
import { FormGroup, FormControl, FormBuilder, Validators } from "@angular/forms";
import { Router } from "@angular/router";

@Component({
  selector: "mavha-login",
  templateUrl: "./login.component.html",
  styleUrls: ["./login.component.scss"]
})
export class LoginComponent implements OnInit {
  public loginForm: FormGroup;

  constructor(protected auth: AuthService, protected fb: FormBuilder, protected router: Router) {}

  ngOnInit() {
    this.loginForm = this.fb.group({
      username: this.fb.control('', [Validators.required, Validators.email]),
      password: this.fb.control('', Validators.required)
    })
  }

  public validateAndLogin() {
    if (this.loginForm.valid) {
      this.onSubmit();
    }
  }
  protected onSubmit() {
    this.auth.login(this.loginForm.value).subscribe(res => {
      this.router.navigate(["/todos"]);
    });
  }
}
