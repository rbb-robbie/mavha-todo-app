import { Component } from '@angular/core';

@Component({
  selector: 'mavha-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'MAvha ToDos';
}
