import { Attachment } from "./attachment.interface";

export enum TODO_STATUS {
  "PENDING" = "PENDING",
  "DONE" = "DONE"
}
export interface Todo {
  id: number;
  description: string;
  status: TODO_STATUS;
  attachments: Attachment[];
}
