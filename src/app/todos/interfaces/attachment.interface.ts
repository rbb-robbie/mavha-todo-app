export interface Attachment {
  id: number;
  filename: string;
  path: string;
  filepath: string;
  [key: string]: any;
}
