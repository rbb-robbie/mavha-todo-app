import { Injectable } from "@angular/core";
import { HttpClient, HttpEventType, HttpHeaders } from "@angular/common/http";
import { Subject } from "rxjs";
import { map } from "rxjs/operators";
import { environment } from "src/environments/environment";
import { Todo } from "./interfaces/todo.interface";
import { AuthService } from "../auth/auth.service";

@Injectable({
  providedIn: "root"
})
export class TodosService {
  protected todos: Subject<Todo[]> = new Subject();
  protected loadedTodo: Subject<Todo> = new Subject();

  constructor(protected http: HttpClient, protected auth: AuthService) {}

  public list(filters?: { [key: string]: string }) {
    this.http
      .get(`${environment.apiUrl}/todos`, { params: filters })
      .subscribe((response: { status: number; data: Todo[] }) => {
        this.todos.next(response.data);
      });
    return this.todos;
  }

  public todo(id?: number) {
    if (id) {
      this.http
        .get(`${environment.apiUrl}/todos/${id}`)
        .subscribe((response: { status: number; data: Todo }) => {
          this.loadedTodo.next(response.data);
        });
    }
    return this.loadedTodo;
  }

  public unloadTodo() {
    this.loadedTodo.next(null);
  }

  public create(values) {
    const holder: Subject<Todo> = new Subject();
    this.http
      .post(`${environment.apiUrl}/todos`, values)
      .subscribe((response: { status: number; data: Todo }) => {
        this.loadedTodo.next(response.data);
        this.list();
        holder.next(response.data);
      });
    return holder;
  }

  public changeStatus(id: number, status: boolean) {
    this.http
      .patch(`${environment.apiUrl}/todos/${id}/status`, {
        status: status ? "DONE" : "PENDING"
      })
      .subscribe(() => {
        this.todo(id);
        this.list();
      });
  }

  public edit(todo: Todo) {
    const holder: Subject<Todo> = new Subject();
    this.http
      .put(`${environment.apiUrl}/todos/${todo.id}`, todo)
      .subscribe((response: { status: number; data: Todo }) => {
        holder.next(response.data);
      });
    return holder;
  }

  public delete(id: number) {
    const holder: Subject<void> = new Subject();
    this.http.delete(`${environment.apiUrl}/todos/${id}`).subscribe(
      () => {
        this.unloadTodo();
        this.list();
      },
      err => {},
      () => holder.next()
    );
    return holder;
  }
  public uploadAttachment(id, data) {
    const holder = new Subject();
    this.http
      .post<any>(`${environment.apiUrl}/todos/${id}/attachments`, data, {
        headers: new HttpHeaders({ "Content-Type": "multipart/form-data" })
      })
      .subscribe(res => holder.next());
    return holder;
  }
  public deleteAttachment(id, attachmentId) {
    const holder: Subject<void> = new Subject();
    this.http
      .delete(`${environment.apiUrl}/todos/${id}/attachments/${attachmentId}`)
      .subscribe(
        () => {
          this.todo(id);
        },
        err => {},
        () => holder.next()
      );
    return holder;
  }
}
