import { Component, OnInit, ViewChild } from "@angular/core";
import { TodosService } from "../todos.service";
import { Router, ActivatedRoute } from "@angular/router";
import { FormBuilder, FormGroup } from "@angular/forms";
import { Todo } from "../interfaces/todo.interface";
import { environment } from "src/environments/environment";
import { Attachment } from "../interfaces/attachment.interface";
import { MatSlideToggleChange } from "@angular/material/slide-toggle";

@Component({
  selector: "mavha-todo-detail",
  templateUrl: "./todo-detail.component.html",
  styleUrls: ["./todo-detail.component.scss"]
})
export class TodoDetailComponent implements OnInit {
  public todo: Todo;
  public form: FormGroup;

  @ViewChild("file", { static: false }) fileUploader;

  constructor(
    protected todos: TodosService,
    protected router: Router,
    protected route: ActivatedRoute,
    protected fb: FormBuilder
  ) {}

  ngOnInit() {
    this.route.params.subscribe(params =>
      this.todos.todo(params.id).subscribe((t: Todo) => (this.todo = t))
    );
    this.form = this.fb.group({
      attachment: [""]
    });
  }

  public detachFile(attachment: Attachment) {
    this.todos.deleteAttachment(this.todo.id, attachment.id);
  }

  public changeTodoStatus(value: MatSlideToggleChange) {
    this.todos.changeStatus(this.todo.id, value.checked);
  }

  public triggerFileUpload() {
    this.fileUploader.nativeElement.click();
  }
  public attachFile(event) {
    const formData = new FormData();
    formData.append("attachment", event.target.files[0]);
    this.todos.uploadAttachment(this.todo.id, formData);
  }

  buildFilePath(filepath) {
    return `localhost:8000${filepath}`;
  }
}
