import { Component, OnInit, Input, Output, EventEmitter } from "@angular/core";
import { Todo } from "../interfaces/todo.interface";

@Component({
  selector: "mavha-todo-list-item",
  templateUrl: "./todo-list-item.component.html",
  styleUrls: ["./todo-list-item.component.scss"]
})
export class TodoListItemComponent implements OnInit {
  @Input() todo: Todo;
  @Output() onDelete: EventEmitter<any> = new EventEmitter();

  public ngOnInit() {}

  deleteTodo(event: Event){
    event.stopPropagation();
    this.onDelete.emit();
  }
}
