import { Component, OnInit } from "@angular/core";
import { TodosService } from "../todos.service";
import { Todo } from "../interfaces/todo.interface";
import { Router } from "@angular/router";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";

@Component({
  selector: "mavha-todos-list",
  templateUrl: "./todos-list.component.html",
  styleUrls: ["./todos-list.component.scss"]
})
export class TodosListComponent implements OnInit {
  public todoList: Todo[] = [];
  public todo: Todo;

  public filters: FormGroup;
  public todoForm: FormGroup;
  public statusOptions: String[] = ["PENDING", "DONE"];

  constructor(
    protected todos: TodosService,
    protected router: Router,
    protected fb: FormBuilder
  ) {}

  ngOnInit() {
    this.todos.list().subscribe(todos => {
      this.todoList = todos.sort((a, b) => (a.status === "DONE" ? 1 : -1));
    });
    this.todos.todo().subscribe(t => (this.todo = t));

    this.filters = this.fb.group({
      description: this.fb.control(""),
      status: this.fb.control("")
    });
    this.todoForm = this.fb.group({
      description: this.fb.control("", Validators.required)
    });
    // IMPLEMENT BOUNCER
    this.filters.valueChanges.subscribe(value => this.todos.list(value));
  }

  public createNew(event: KeyboardEvent) {
    if (event.key === "Enter") {
      this.todos.create(this.todoForm.value).subscribe(() => {
        this.todoForm.reset();
        this.todoForm.get('description').setErrors(null);
        this.todoForm.updateValueAndValidity();
      });
    }
  }

  public loadTodo(todo: Todo) {
    this.router.navigate([`todos/${todo.id}`]);
  }

  public deleteTodo(todo: Todo) {
    this.todos.delete(todo.id).subscribe(() => {
      return;
    });
  }
}
