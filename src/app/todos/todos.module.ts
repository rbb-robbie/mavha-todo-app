import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";

import { TodosRoutingModule } from "./todos-routing.module";
import { TodosListComponent } from "./todos-list/todos-list.component";
import { TodoListItemComponent } from "./todos-list/todo-list-item.component";
import { TodosService } from "./todos.service";

import { MatInputModule } from "@angular/material/input";
import { MatSelectModule } from "@angular/material/select";
import { MatListModule } from "@angular/material/list";
import { MatIconModule } from "@angular/material/icon";
import { MatCardModule } from "@angular/material/card";
import { MatSlideToggleModule } from "@angular/material/slide-toggle";

import { TodoDetailComponent } from "./todo-detail/todo-detail.component";
import { ReactiveFormsModule } from "@angular/forms";

@NgModule({
  declarations: [
    TodosListComponent,
    TodoListItemComponent,
    TodoDetailComponent
  ],
  imports: [
    CommonModule,
    TodosRoutingModule,
    ReactiveFormsModule,
    MatInputModule,
    MatSelectModule,
    MatListModule,
    MatIconModule,
    MatCardModule,
    MatSlideToggleModule
  ],
  providers: [TodosService]
})
export class TodosModule {}
